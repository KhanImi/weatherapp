import React from 'react'
import HomeScreen from '../scr/screens/HomeScreen'

import renderer from 'react-test-renderer'

test('render correct', () => {
  const tree = renderer.create(<HomeScreen />).toJSON()
  expect(tree).toMatchSnapshot()
})
