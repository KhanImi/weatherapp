import React from 'react'
import CityNameScreen from '../scr/screens/CityNameScreen'

import renderer from 'react-test-renderer'

test('render correct', () => {
  const tree = renderer.create(<CityNameScreen />).toJSON()
  expect(tree).toMatchSnapshot()
})
