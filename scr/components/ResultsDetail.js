import React from 'react'
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native'
import { Image } from 'react-native-elements'

const ResultsDetail = ({ result }) => {
  /**
   * Check if return result has any empty array or null
   */
  if (!result.current) {
    return null
  }
  const { fonts } = styles
  const { weather_icons, weather_descriptions, observation_time } = result.current
  const { timezone_id } = result.location
  return (
    <View>
      <Text style={fonts}>{timezone_id} Weather</Text>
      <Image source={{ uri: weather_icons[0] }} style={styles.image} />
      <Text style={fonts}>{weather_descriptions[0]}</Text>
      <Text style={fonts}>Observation Time: {observation_time}</Text>
    </View>
  )
}

export default ResultsDetail

const styles = StyleSheet.create({
  image: {
    width: 64,
    height: 64,
    borderRadius: 4,
    marginBottom: 5
  },
  fonts: {
    fontWeight: 'bold',
    paddingTop: 20,
    paddingBottom: 20
  }
})
