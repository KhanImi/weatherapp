import React, { useEffect } from 'react'
import Geolocation from '@react-native-community/geolocation'
import { StyleSheet, Text, View } from 'react-native'
import { Button } from 'react-native-elements'

import UseResults from '../hooks/UseResults'
import ResultsDetail from '../components/ResultsDetail'

const HomeScreen = ({ navigation }) => {
  const { container } = styles
  const [searchApi, results, errorMessage] = UseResults()
  /**
   * useEffect use for one time call on page load
   */
  useEffect(() => {
    Geolocation.getCurrentPosition(position => {
      const { coords: { latitude, longitude } } = position
      searchApi(`${latitude},${longitude}`)
    }, (error) => {
      console.log(error.message)
    })
  }, [])
  return (
    <View style={container}>
      {errorMessage ? <Text>{errorMessage}</Text> : null}
      <ResultsDetail result={results} />
      <Button
        title="Check Other City Weather"
        onPress={() => navigation.navigate('SelectCity')}
      />
    </View>
  )
}

export default HomeScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 30
  }
})
